// # Let's write a crawler (almost)
// ## What we'll cover today
// - [ ] Setting up your own project
// - [ ] Integrating external dependencies
// - [ ] Doing basic HTTP
// - [ ] The Result type
// - [ ] Default
// - [ ] Implementing traits
// - [ ] Pattern Matching

// # Probably no time for this
// - [ ] Ownership
// - [ ] Spawning threads
// - [ ] Send and Sync
// - [ ] Using Arc and Mutex
// - [ ] The Option type
// - [ ] Scopes
// - [ ] Channels

extern crate hyper;
extern crate html5ever;
extern crate tendril;

use std::io::Read;
use std::thread;
use std::sync::mpsc::channel;
use std::sync::Arc;
use std::sync::Mutex;

use hyper::Client;

use html5ever::tokenizer::TokenSink;
use html5ever::tokenizer::Token;
use html5ever::driver::tokenize_to;
use html5ever::driver::one_input;
use html5ever::tokenizer::Token::TagToken;
use html5ever::tokenizer::TagKind::StartTag;
use html5ever::tokenizer::Tag;

use tendril::Tendril;
use tendril::StrTendril;
use tendril::SendTendril;
use tendril::fmt::UTF8;

struct LinkFinder{
    links: Vec<SendTendril<UTF8>>
}

impl TokenSink for LinkFinder {
    fn process_token(&mut self, token: Token){
        match token {
            TagToken(tag @ Tag{kind: StartTag, ..}) => {
                if tag.name.as_slice() == "a" {
                    for attr in tag.attrs{
                        if attr.name.local.as_slice() == "href" {
                            self.links.push(attr.value.into_send());
                        }
                    }
                }
            }
            _ => ()
        }
    }
}

fn http_get(url: &str) -> String {
    // Create a client.
    let client = Client::new();

    // Creating an outgoing request.
    let mut res = client.get(url).send().unwrap();

    // Read the Response.
    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();

    body
}

fn main() {
    let body = http_get("http://rust-lang.org/");

    let logger = LinkFinder{links: vec![]};

    let logger = tokenize_to(logger, one_input(StrTendril::from(body)), Default::default());

    let data = Arc::new(Mutex::new(logger.links));

    let (tx, rx) = channel();

    for i in 0..10 {
        let data = data.clone();
        let tx = tx.clone();
        thread::spawn(move || {
            loop {
                let url : Tendril<UTF8>;
                {
                    let mut data = data.lock().unwrap();
                    let d = data.pop();
                    if d.is_none(){
                        break;
                    }
                    url = Tendril::<UTF8>::from(d.unwrap());
                }
                println!("Worker number {} downloading {}", i, url);
                thread::sleep_ms(2000);
                println!("Worker number {} finished downloading {}", i, url);
            }
            tx.send(i);
        });
    }

    for _ in 0..10 {
        rx.recv();
    }
}
