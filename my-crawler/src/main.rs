// # Let's write a crawler (almost)
// ## What we'll cover today
// - [x] Setting up your own project
// - [x] Integrating external dependencies
// - [x] Mutability
// - [x] Doing basic HTTP
// - [x] The Result type
// - [x] Default
// - [x] Implementing traits
// - [x] Pattern Matching

// # Probably no time for this
// - [x] Ownership
// - [x] Spawning threads
// - [x] Send and Sync
// - [x] Using Arc and Mutex
// - [x] The Option type
// - [ ] Scopes
// - [ ] Channels

extern crate hyper;
extern crate html5ever;
extern crate tendril;

use std::io::Read;
use std::thread;
use std::sync::mpsc::channel;
use std::sync::Arc;
use std::sync::Mutex;

use html5ever::tokenizer::TokenSink;
use html5ever::tokenizer::Token;
use html5ever::driver::tokenize_to;
use html5ever::driver::one_input;
use html5ever::tokenizer::Token::TagToken;
use html5ever::tokenizer::TagKind::StartTag;
use html5ever::tokenizer::Tag;

use tendril::Tendril;
use tendril::StrTendril;
use tendril::SendTendril;
use tendril::fmt::UTF8;

use hyper::Client;

struct LinkFinder{
    links: Vec<SendTendril<UTF8>>
}

impl TokenSink for LinkFinder {
    fn process_token(&mut self, token: Token){
        match token {
            TagToken(tag @ Tag{kind: StartTag, ..}) => {
                if tag.name.as_slice() == "a" {
                    for attr in tag.attrs {
                        if attr.name.local.as_slice() == "href" {
                            self.links.push(attr.value.into_send());
                        }
                    }
                }
            }
            _ => ()
        }
    }
}

fn main() {
    let client = Client::new();

    let mut res = client.get("http://rust-lang.org").send().unwrap();

    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();

    let mut sink = LinkFinder{links: vec!()};

    let sink = tokenize_to(sink, one_input(StrTendril::from(body)), Default::default());

    let mut data = Arc::new(Mutex::new(sink.links));

    for i in 0..10 {
        let data = data.clone();
        thread::spawn(move || {
            loop {
                let url: Tendril<UTF8>;
                {
                    let mut data = data.lock().unwrap();
                    let d = data.pop();
                    if d.is_none(){
                        break;
                    }
                    url = Tendril::<UTF8>::from(d.unwrap());
                }
                println!("Thread {} is now downloading {}", i, url);
                thread::sleep_ms(2000); // TODO actually download
                println!("Thread {} has downloaded {}", i, url);
            }
        });
    }
}
