extern crate hyper;

use hyper::client::Client;
use std::io::Read;

fn main() {
    let client = Client::new();

    let mut res = client.get("http://rust-lang.org").send().unwrap();
    assert_eq!(res.status, hyper::Ok);
    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();
    //assert_eq!(2, 3);
    println!("{}", body);
}

#[test]
fn test_me (){
    assert_eq!(2, 2);
}
