// PATTERN MATCHING

struct Foo {
    a: isize,
    b: String
}

pub fn main() {
    let a = 2000;
    let foo = Foo{a: 2000, b: "Hello".to_string()};
    match a  {
        5 | 7 => println!("It's a 5 or a 7"),
        42 => println!("It's a 42"),
        x if x > 1000 => println!("Incredible, it's a {}", x),
        x => println!("Wow, it's a {}", x),
    };
    match foo {
        Foo{a: 2000, ref b} => println!("a is 2000, yo, b:{}", b),
        Foo{a, ref b} => println!("a: {}, b:{}", a, b)
    };
    let Foo{a: ref x, b: ref y} = foo;
    println!("{}{}",x,y);
}

