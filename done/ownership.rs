// OWNERSHIP AND LIFETIMES EXAMLPE

struct Bar<'a> {
    b : &'a mut Foo
}

struct Foo {
    a : isize
}

pub fn main() {
    println!("Starting main");

    let mut foo = Foo{a:42};
    let bar = Bar{b:&mut foo};
    let bar2 = Bar{b:&mut foo};

    set_a(bar.b, 100);

    println!("Ending main, a is a {}", bar.b.a);
}

fn set_a(foo: &mut Foo, new_a : isize) {
    foo.a = new_a;
}

