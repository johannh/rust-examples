struct Person {
    name: String,
    age: i32
}

fn main() {
    let mut kevin = Person{name: "Kevin".to_string(), age: 5};

    happy_birthday(&mut kevin);
    //print_kevin(&kevin);

    let name = get_name(&kevin);

    println!("{}", name);

}

fn get_name(p: &Person) -> String{
    return p.name.to_string();
}

fn happy_birthday(p: &mut Person){
    p.age += 1;
}

fn print_kevin(k: &Person){
    println!("Hello {}", k.name);
}
