// # Let's write a crawler (almost)
// ## What we'll cover today
// - [x] Setting up your own project
// - [x] Integrating external dependencies
// - [x] Mutability
// - [x] Doing basic HTTP
// - [x] The Result type
// - [ ] Default
// - [x] Implementing traits
// - [x] Pattern Matching

// # Probably no time for this
// - [ ] Ownership
// - [ ] Spawning threads
// - [ ] Send and Sync
// - [ ] Using Arc and Mutex
// - [ ] The Option type
// - [ ] Scopes
// - [ ] Channels

extern crate hyper;
extern crate html5ever;
extern crate tendril;

use hyper::Client;
use std::io::Read;

use html5ever::tokenizer::TokenSink;
use html5ever::tokenizer::Token;
use html5ever::driver::tokenize_to;
use html5ever::driver::one_input;
use html5ever::tokenizer::Token::TagToken;
use html5ever::tokenizer::TagKind::StartTag;
use html5ever::tokenizer::Tag;

use tendril::Tendril;
use tendril::StrTendril;
use tendril::SendTendril;
use tendril::fmt::UTF8;

struct TokenLogger;

impl TokenSink for TokenLogger {
    fn process_token(&mut self, token: Token){
        match token {
            TagToken(tag @ Tag{kind: StartTag, ..}) => {
                if tag.name.as_slice() == "a" {
                    for attr in tag.attrs {
                        if attr.name.local.as_slice() == "href" {
                            println!("{:?}", attr.value);
                        }
                    }
                }
            }
            _ => (),
        }
    }
}

fn main() {
    let client = Client::new();

    // DON'T TRY TO USE UNWRAP AT HOME
    let mut res = client.get("http://rust-lang.org").send().unwrap();

    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();

    println!("{}", body);

    let sink = TokenLogger;

    tokenize_to(sink, one_input(StrTendril::from(body)), Default::default());
}
