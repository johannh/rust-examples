fn main(){
    let args : Vec<_> = std::env::args().collect();
    let arg1 : &str = &args[1];
    let num = parse_number(arg1).unwrap_or(0);
    println!("{}", num);
}

fn parse_number(input: &str) -> Result<i32, String>{
    match input.parse::<i32>(){
        Ok(thing) => Ok(thing),
        Err(_) => Err("What are you doing?".to_string())
    }
}

