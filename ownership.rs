struct Bar<'a>{
    my_foo : &'a mut Foo
}
struct Foo{
    num : i32
}

fn main(){
    let mut foo = Foo{num:5};

    increment_num(&mut foo);
    let bar = Bar{my_foo:&mut foo};

    println!("{}", bar.my_foo.num);
}

fn increment_num(foo: &mut Foo){
    foo.num = foo.num + 1;
}
